<?php
/**
 * Amass Market
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 *
 * Module index.php
 */

defined('APP_DEBUG') or define('APP_DEBUG', true);

require(__DIR__ . '/../vendor/autoload.php');

use Amass\MeestExpress\Client;

/**
 * login to meest Express === user1
 * @var $login string
 */
$login = 'user1';

/**
 * password to to meest Express === pass1
 * @var $password string
 */
$password = 'pass1';

/**
 * get new Object
 * Class Client єдина точка входу все остальне челез цей клас
 * @var $client object Client
 */
$client = new Client();

/**
 * set login
 */
$client->setLogin($login);

/**
 * set password
 */
$client-> setPassword($password);

$country  = $client->getDefaultCountry();

$isHasBranch = "IsBranchInCity = '1'";

$sql = $isHasBranch. " AND ". $country ." AND (DescriptionUA like 'іва%' OR DescriptionRU like 'ива%')";

echo '<pre>'; print_r($client->getCity($sql)->getObjectMappedResult());

<?php
/**
* This file is part of the "Meest Express" API PHP Client
*
 * (c) Igor Moskal  (Amass Advance) <wearde.studio@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
 */


namespace Amass\MeestExpress\mapping;

/**
 * @author Amass Advance <wearde.studio@gmail.com>
 * @link http://amass.pp.ua
 */

class Object
{
    /**
     * Returns the fully qualified name of this class.
     * @return string the fully qualified name of this class.
     */
    public static function className()
    {
        return get_called_class();
    }

    /**
     * Returns the value of an object property.
     *
     * Do not call this method directly as it is a PHP magic method that
     * will be implicitly called when executing `$value = $object->property;`.
     * @param string $property the property name
     * @throws \InvalidArgumentException if the property is not defined
     * @return mixed the property value
     *
     */
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }else {
            throw new \InvalidArgumentException('Getting unknown property: ' . get_class($this) . '::' . $property);
        }
    }
    /**
     * Sets value of an object property.
     *
     * Do not call this method directly as it is a PHP magic method that
     * will be implicitly called when executing `$object->property = $value;`.
     * @param string $property the property name or the event name
     * @param mixed $value the property value
     * @throws \InvalidArgumentException if the property is not defined
     */
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }else {
            throw new \InvalidArgumentException('Setting unknown property: ' . get_class($this) . '::' . $property);
        }
    }
}
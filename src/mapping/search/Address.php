<?php
/**
 * This file is part of the "Meest Express" API PHP Client
 *
 * (c) Igor Moskal  (Amass Advance) <wearde.studio@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace Amass\MeestExpress\mapping\search;
use Amass\MeestExpress\mapping\Object;

/**
 * @author Amass Advance <wearde.studio@gmail.com>
 * @link http://amass.pp.ua
 */

class Address extends Object
{
    public $uuid;
    public $DescriptionUA;
    public $DescriptionRU;
    public $DescriptionEN;
    public $StreetTypeUA;
    public $StreetTypeRU;
    public $ZIP;
    public $Cityuuid;
    public $Districtuuid;
    public $MainBranchUUID;
    public $DateChange;
}
<?php
/**
 * This file is part of the "Meest Express" API PHP Client
 *
 * (c) Igor Moskal  (Amass Advance) <wearde.studio@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Amass\MeestExpress\mapping\search;
use Amass\MeestExpress\mapping\Object;

/**
 * @author Amass Advance <wearde.studio@gmail.com>
 * @link http://amass.pp.ua
 */

class Branch extends Object
{
    public $UUID;
    public $DescriptionUA;
    public $DescriptionRU;
    public $RegionUUID;
    public $RegionDescriptionUA;
    public $RegionDescriptionRU;
    public $DistrictUUID;
    public $DistrictDescriptionUA;
    public $DistrictDescriptionRU;
    public $CityUUID;
    public $CityDescriptionUA;
    public $CityDescriptionRU;
    public $StreetUUID;
    public $StreetTypeUA;
    public $StreetDescriptionUA;
    public $StreetTypeRU;
    public $StreetDescriptionRU;
    public $House;
    public $Flat;
    public $AddressMoreInformation;
    public $Limitweight;
    public $Branchtype;
    public $Latitude;
    public $Longitude;
    public $BranchCode;
    public $WorkingHours;
}
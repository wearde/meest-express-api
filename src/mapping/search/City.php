<?php
/**
 * This file is part of the "Meest Express" API PHP Client
 *
 * (c) Igor Moskal  (Amass Advance) <wearde.studio@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Amass\MeestExpress\mapping\search;
use Amass\MeestExpress\mapping\Object;

/**
 * @author Amass Advance <wearde.studio@gmail.com>
 * @link http://amass.pp.ua
 */

class City extends Object
{
    public $uuid;
    public $Countryuuid;
    public $Regionuuid;
    public $Districtuuid;
    public $DescriptionUA;
    public $DescriptionRU;
    public $Branchuuid;
    public $SeachName;
    public $IsBranchInCity;
    public $DistrictDescriptionUA;
    public $DistrictDescriptionRU;
    public $RegionDescriptionUA;
    public $RegionDescriptionRU;
    public $Sun;
    public $Mon;
    public $Tue;
    public $Wed;
    public $Thu;
    public $Fri;
    public $Sat;
}
<?php
/**
 * This file is part of the "Meest Express" API PHP Client
 *
 * (c) Igor Moskal  (Amass Advance) <wearde.studio@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Amass\MeestExpress\mapping\search;
use Amass\MeestExpress\mapping\Object;

/**
 * @author Amass Advance <wearde.studio@gmail.com>
 * @link http://amass.pp.ua
 */

class Country extends Object
{
    /**
     * @var string $uuid  Uniqueidentifier Number
     */
    public $uuid;

    /**
     * @var string $DescriptionUA
     */
    public $DescriptionUA;

    /**
     * @var string $DescriptionRU
     */
    public $DescriptionRU;

    /**
     * @var string $DescriptionEn
     */
    public $DescriptionEN;

    /**
     * @var string $AlfaCode1
     */
    public $AlfaCode1;

    /**
     * @var string $AlfaCode2
     */
    public $AlfaCode2;


}
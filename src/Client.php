<?php
/**
 * This file is part of the "Meest Express" API PHP Client
 *
 * (c) Igor Moskal  (Amass Advance) <wearde.studio@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Amass\MeestExpress;
use Amass\MeestExpress\API\Search\Address;
use Amass\MeestExpress\API\Search\Branch;
use Amass\MeestExpress\API\Search\City;
use Amass\MeestExpress\API\Search\Country;
use Amass\MeestExpress\API\Search\ShipmentFormats;
use Amass\MeestExpress\API\Shipment\CalculateShipments;
use Amass\MeestExpress\API\Shipment\CreateRegister;
use Amass\MeestExpress\API\Shipment\CreateShipments;
use Amass\MeestExpress\API\Shipment\DeleteShipments;
use Amass\MeestExpress\Component\Component;

/**
 * "Meest Express" API PHP Client
 *
 * @author Igor Moskal  (Amass Advance) <wearde.studio@gmail.com>
 * @see    https://meest-express.com.ua/ua/services/api "Meest Express" API Documentation
 */
class Client extends Component
{
    /**
     * @var string $_login Login
     */
    private $_login;
    /**
     * @var string $_password Password
     */
    private $_password;
    /**
     * @var string $_defaultCountry
     */
    private $_defaultCountry = "Countryuuid = 'C35B6195-4EA3-11DE-8591-001D600938F8'";

    /**
     * Set public key
     *
     * @param string $login Locale
     */
    public function setLogin($login)
    {
        $this->_login = $login;
    }

    /**
     * Set private key
     *
     * @param string $pass Locale
     */
    public function setPassword($pass)
    {
        $this->_password = $pass;
    }

    /**
     * Set public key
     *
     * @param string $data Default Country
     */
    public function setDefaultCountry($data)
    {
        $this->_defaultCountry = $data;
    }


    /**
     * Get public key
     *
     */
    public function getLogin()
    {
       return $this->_login;
    }

    /**
     * Get private key
     *
     */
    public function getPassword()
    {
      return $this->_password;
    }

    /**
     * Get default Country
     *
     */
    public function getDefaultCountry()
    {
        return $this->_defaultCountry;
    }
    /**
     * Get list of areas (Countries)
     *
     * Method 1.2 from API documentation
     * @param string $where
     * @return API\Search\Country
     */
    public function getCountries($where)
    {
        return new Country($this->getLogin(), $this->getPassword(), $where);
    }

    /**
     * Get list of Regions
     *
     * Method 1.3 from API documentation
     * @param string $where
     */
    public function getRegions($where)
    {
        /**@todo method */
    }

    /**
     * Get list of areas (cities)
     *
     * Method 1.5 from API documentation
     * @param string $where
     * @return API\Search\City
     */
    public function getCity($where)
    {
        return new City($this->getLogin(), $this->getPassword(), $where);
    }

    /**
     * Get list of areas (addresses)
     *
     * Method 1.6 from API documentation
     * @param string $where
     * @return API\Search\Address
     */
    public function getAddress($where)
    {
        return new Address($this->getLogin(), $this->getPassword(), $where);
    }

    /**
     * Get list of areas (branches)
     *
     * Method 1.7 from API documentation
     * @param string $where
     * @return API\Search\Branch
     */
    public function getBranch($where)
    {
        return new Branch($this->getLogin(), $this->getPassword(), $where);
    }

    /**
     * Get list of ShipmentFormats
     *
     * Method 1.9 from API documentation
     * @param string $where
     * @return API\Search\ShipmentFormats
     */
    public function getShipmentFormats($where)
    {
        return new ShipmentFormats($this->getLogin(), $this->getPassword(), $where);
    }
    /**
     * Crete shipment
     *
     * Method 2.2 from API documentation
     * @param string $request
     * @return API\Shipment\CreateShipments
     */
    public function getCreateShipments($request)
    {
        return new CreateShipments($this->getLogin(), $this->getPassword(), $request);
    }

    /**
     * Delete shipment
     *
     * Method 2.3 from API documentation
     * @param string $request
     * @return API\Shipment\DeleteShipments
     */
    public function getDeleteShipments($request)
    {
        return new DeleteShipments($this->getLogin(), $this->getPassword(), $request);
    }

    /**
     * Register shipment
     *
     * Method 2.4 from API documentation
     * @param string $request
     * @return API\Shipment\CreateRegister
     */
    public function getCreateRegister($request)
    {
        return new CreateRegister($this->getLogin(), $this->getPassword(), $request);
    }

    /**
     * CalculateShipments
     *
     * Method 2.5 from API documentation
     * @param string $request
     * @return API\Shipment\CalculateShipments
     */
    public function getCalculateShipments($request)
    {
        return new CalculateShipments($this->getLogin(), $this->getPassword(), $request);
    }
}

<?php
/*
 * This file is part of the "Meest Express" API PHP Client
 *
 * (c) Igor Moskal  (Amass Advance) <wearde.studio@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Amass\MeestExpress\API;

/**
 * Abstract API Method Class
 *
 * @author Igor Moskal  (Amass Advance) <wearde.studio@gmail.com>
 *
 * @abstract
 */
abstract class AbstractApiMethod
{

    /**
     * @var string $apiUrl apiUrl API URL
     * @static
     */
    protected $apiUrl;

    protected $mappingClassName;

    /**
     * @var string $function name function
     * @static
     */
    protected $login = '';

    /**
     * @var string $function name function
     * @static
     */
    protected $password = '';

    /**
     * @var string $function name function
     * @static
     */
    protected $function = '';

    /**
     * @var string $xml build xml string
     */
    protected $xml;

    /** Constructor.
    * The default implementation
    * - Initializes the Api Url
    * - call the parent implementation at the begin of the constructor.
    */
    protected function __construct()
    {
        $this->setSetApiUrl();
    }


    /**
     * Method returns object mapped result depends on each API call
     *
     * @abstract
     */
    abstract public function getObjectMappedResult();

    /**
     * Method set upi url
     *
     * @abstract
     */
    abstract protected function setSetApiUrl();

    /**
     *
     * @return string API URL
     */
    protected function getApiUrl()
    {
        return $this->apiUrl;
    }

    /**
     * Build API Method URL
     *
     * @return string API Method URL
     */
    protected function buildMethodApiUrl()
    {
        return $this->getApiUrl();
    }

    /**
     * @return mixed Raw response
     */
    protected function getCurl()
    {
        try{
            $ch = curl_init();
            if (false === $ch){
                throw new \Exception('failed to initialize');
            }
            curl_setopt($ch, CURLOPT_URL, $this->buildMethodApiUrl());
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $this->xml);  // XML post
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, 0);

            $headers = array();
            $headers[] = 'Content-Type: text/xml';

            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $server_output = curl_exec($ch);

            if (false === $server_output) {
                throw new \Exception(curl_error($ch), curl_errno($ch));
            } else{
                curl_close($ch);
                return $server_output;
            }
        }catch(\Exception $e) {
            trigger_error(sprintf('Curl failed with error #%d: %s',$e->getCode(), $e->getMessage()),E_USER_ERROR);
        }
    }

    /**
     * Get raw response
     *
     * @return mixed Raw response
     */
    protected function getRawResponse()
    {
        return $this->getCurl();
    }

    /**
     * Get array result
     *
     * @throws \Exception
     *
     * @return mixed Raw array result
     */
    protected function getArrayResult()
    {
        $response = $this->getRawResponse();
        $xml = new \SimpleXMLElement($response);
        return json_decode(json_encode($xml), true);
    }

    /**
     * @param array $data
     * @return array objects
     */
    protected function getArrayMappingObject($data)
    {
        $object = new $this->mappingClassName;
        foreach($data as $key => $value)
        {
            $object->{$key} = $value;
        }
        return $object;
    }
}

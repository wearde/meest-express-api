<?php
/**
 * This file is part of the "Meest Express" API PHP Client
 *
 * (c) Igor Moskal  (Amass Advance) <wearde.studio@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Amass\MeestExpress\API\Search;

/**
 * @author Amass Advance <wearde.studio@gmail.com>
 * @link http://amass.pp.ua
 */

use Amass\MeestExpress\API\AbstractApiMethod;

class SearchApiMethod extends AbstractApiMethod
{

    /**
     * @var string $where name body requests
     * @static
     */
    protected $where = '';

    /**
     * @var string $order sort order
     * @static
     */
    protected $order = '';

    /**
     * Constructor.
     * The default implementation
     * - Initializes the object with the given configuration
     *
     * If this method is overridden in a child class, it is recommended that
     * - call the parent implementation at the begin of the constructor.
     *
     * @param string $login
     * @param string $password
     * @param string $where
     * @param string $order
     */
    public function __construct($login, $password, $where, $order = '')
    {
        parent::__construct();

        $this->login = $login;
        $this->password = $password;
        $this->function = 'Default';
        $this->where = $where;
        $this->order = $order;
    }
    /**
     * Set api Url
     */
    protected function setSetApiUrl()
    {
       $this->apiUrl = 'http://api1c.meest-group.com/services/1C_Query.php';
    }

    /**
     * Method returns object mapped result depends on each API call
     */
    public function getObjectMappedResult(){}

    public function getArrayResult()
    {
        return parent::getArrayResult(); // TODO: Change the autogenerated stub
    }

    public function getRawResponse()
    {
        return parent::getRawResponse(); // TODO: Change the autogenerated stub
    }

    /**
     * @param array $array
     *
     * @return array objects
     */
    protected function setMappingObject($array)
    {
        $result = [];
        foreach($array['result_table'] as $data)
        {
            if($data)
            {
                if (count($data, COUNT_RECURSIVE) != count($data))
                {
                    foreach($data as $item)
                    {
                        if(is_array($item))
                        {
                            array_push($result, $this->getArrayMappingObject($item));
                        }
                    }
                }else{
                    array_push($result, $this->getArrayMappingObject($data));
                }
            }
        }
        return $result;
    }

    /**
     * Method returns build xml
     *
     */
    protected function buildXml()
    {
        $this->xml = '<?xml version="1.0" encoding="utf-8"?>
                <param>
                    <login>' . $this->login .'</login>
                    <function>' . $this->function .'</function>
                    <where>' . $this->where .'</where>
                    <order>' . $this->order .'</order>
                    <sign>' . $this->sign() .'</sign>
                </param>';
    }

    protected function sign()
    {
       return md5($this->login . $this->password . $this->function . $this->where . $this->order);
    }

}
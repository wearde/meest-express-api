<?php
/**
 * This file is part of the "Meest Express" API PHP Client
 *
 * (c) Igor Moskal  (Amass Advance) <wearde.studio@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Amass\MeestExpress\API\Search;

/**
 * @author Amass Advance <wearde.studio@gmail.com>
 * @link http://amass.pp.ua
 */

class ShipmentFormats extends SearchApiMethod
{
    /**
     * Constructor.
     *
     * @param string $login
     * @param string $password
     * @param string $where
     * @param string $order
     */
    public function __construct($login, $password, $where, $order = '')
    {
        parent::__construct($login, $password, $where, $order = '');
        $this->function = 'ShipmentFormats';
        $this->buildXml();
    }
    /**
     * Get object mapped result
     *
     * @return array
     */
    public function getObjectMappedResult()
    {
        $this->mappingClassName = \Amass\MeestExpress\mapping\search\ShipmentFormats::className();
        return $this->setMappingObject($this->getArrayResult());
    }
}
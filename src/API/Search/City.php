<?php
/**
 * This file is part of the "Meest Express" API PHP Client
 *
 * (c) Igor Moskal  (Amass Advance) <wearde.studio@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Amass\MeestExpress\API\Search;

use Amass\MeestExpress\mapping\search\City as MappingCity;

/**
 * @author Amass Advance <wearde.studio@gmail.com>
 * @link http://amass.pp.ua
 */

class City extends SearchApiMethod
{
    /**
     * Constructor.
     *
     * @param string $login
     * @param string $password
     * @param string $where
     * @param string $order
     */
    public function __construct($login, $password, $where, $order = '')
    {
        parent::__construct($login, $password, $where, $order = '');
        $this->function = 'City';
        $this->buildXml();
    }
    /**
     * Get object mapped result
     * @return array
     */
    public function getObjectMappedResult()
    {
        $this->mappingClassName = MappingCity::className();
        return $this->setMappingObject($this->getArrayResult());
    }
}
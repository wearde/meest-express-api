<?php
/**
 * This file is part of the "Meest Express" API PHP Client
 *
 * (c) Igor Moskal  (Amass Advance) <wearde.studio@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Amass\MeestExpress\API\Shipment;

/**
 * @author Amass Advance <wearde.studio@gmail.com>
 * @link http://amass.pp.ua
 */

use Amass\MeestExpress\API\AbstractApiMethod;

class ShipmentApiMethod extends AbstractApiMethod
{

    /**
     * @var string $request
     * @static
     */
    protected $request = '';

    /**
     * @var string $request_id
     * @static
     */
    protected $request_id = '';

    /**
     * @var string $wait
     * @static
     */
    protected $wait = '';

    /**
     * Constructor.
     * The default implementation
     * - Initializes the object with the given configuration
     *
     * If this method is overridden in a child class, it is recommended that
     * - call the parent implementation at the begin of the constructor.
     *
     * @param string $login
     * @param string $password
     * @param string $request
     * @param string $request_id
     * @param string $wait
     */
    public function __construct($login, $password, $request, $request_id = '', $wait = '1')
    {
        parent::__construct();
        $this->login = $login;
        $this->password = $password;
        $this->function = 'Default';
        $this->request = $request;
        $this->request_id = $request_id;
        $this->wait = $wait;
    }

    /**
     * Set api Url
     */
    protected function setSetApiUrl()
    {
        $this->apiUrl = 'http://api1c.meest-group.com/services/1C_Document.php';
    }

    /**
     * Method returns object mapped result depends on each API call
     */
    public function getObjectMappedResult(){}

    protected function setMappingObject($array)
    {
        $result = [];
        foreach($array['result_table'] as $data)
        {
            if($data)
            {
                if (count($data, COUNT_RECURSIVE) != count($data))
                {
                    foreach($data as $item)
                    {
                        if(is_array($item))
                        {
                            array_push($result, $this->getArrayMappingObject($item));
                        }
                    }
                }else{
                    array_push($result, $this->getArrayMappingObject($data));
                }
            }
        }
        return $result;
    }

    /**
     * Method returns build xml
     *
     */
    protected function buildXml()
    {
        $this->xml = '<?xml version="1.0" encoding="utf-8"?>
                <param>
                    <login>' . $this->login .'</login>
                    <function>' . $this->function .'</function>
                    <request>' . $this->request .'</request>
                    <request_id>' . $this->request_id .'</request_id>
                     <wait>' . $this->wait .'</wait>
                    <sign>' . $this->sign() .'</sign>
                </param>';
    }

    protected function sign()
    {
        return md5($this->login . $this->password . $this->function . $this->request . $this->request_id . $this->wait);
    }
}
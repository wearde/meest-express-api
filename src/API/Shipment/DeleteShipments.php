<?php
/**
 * This file is part of the "Meest Express" API PHP Client
 *
 * (c) Igor Moskal  (Amass Advance) <wearde.studio@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Amass\MeestExpress\API\Shipment;

/**
 * @author Amass Advance <wearde.studio@gmail.com>
 * @link http://amass.pp.ua
 */

class DeleteShipments extends ShipmentApiMethod
{
    /**
     * Constructor.
     *
     * @param string $login
     * @param string $password
     * @param string $request
     * @param string $request_id
     * @param string $wait
     */
    public function __construct($login, $password, $request, $request_id = '', $wait = '1')
    {
        parent::__construct($login, $password, $request, $request_id, $wait);
        $this->function = 'DeleteShipments';
        $this->buildXml();
    }
    /**
     * Get object mapped result
     *
     * @return array
     */
    public function getObjectMappedResult()
    {
        $this->mappingClassName = \Amass\MeestExpress\mapping\shipment\DeleteShipments::className();
        return $this->getArrayResult();
    }
}
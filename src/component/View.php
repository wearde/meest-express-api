<?php
/**
 * This file is part of the "Meest Express" API PHP Client
 *
 * (c) Igor Moskal  (Amass Advance) <wearde.studio@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Amass\MeestExpress\Component;

/**
 * @author Amass Advance <wearde.studio@gmail.com>
 * @link http://amass.pp.ua
 */

use Amass\MeestExpress\mapping\Object;

class View extends Object
{
    /** @var $_fileName - patch*/
    private $_fileName;

    /**
     * Renders a view, outputting it to the client.
     * @param string $slug like part of method/method
     * @param array $data vars
     * @return string part xml
     */
    public function render($slug, $data = null)
    {
        // Get the expected path of the view template
        $this->_fileName = __DIR__.'/../views/' . $slug . '.php';
       return $this->renderFile($data);
    }

    /**
     * Renders the body of a view.
     * @param $data
     * @throws \Exception error
     * @return mixed
     */
    public function renderFile($data) {
        if(is_readable($this->_fileName)){
            ob_start();
            ob_implicit_flush(false);

            if(isset($data))
                extract($data, EXTR_OVERWRITE);

            require($this->_fileName);

            return ob_get_clean();
        }
        else
            return  new \Exception('File name:'. $this->_fileName .' not exist');
    }
}
<?php

/**
 * This file is part of the "Meest Express" API PHP Client
 *
 * (c) Igor Moskal  (Amass Advance) <wearde.studio@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Amass\MeestExpress\Component;

/**
 * @author Amass Advance <wearde.studio@gmail.com>
 * @link http://amass.pp.ua
 */

use Amass\MeestExpress\mapping\Object;

class Component extends Object
{
    protected $view;

    /**
     * Returns the view object that can be used to render views or view files.
     * The [[render()]], methods will use
     * this view object to implement the actual view rendering.
     * @return View| View the view object that can be used to render views or view files.
     */
    public function getView()
    {
        if ($this->view === null) {
            $this->view = new View();
        }
        return $this->view;
    }
}
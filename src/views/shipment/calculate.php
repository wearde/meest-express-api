<?php
/**
 * @author Amass Advance <wearde.studio@gmail.com>
 * @link http://amass.pp.ua
 */
/**
 * @see https://wiki.meest-group.com/index.php/en/2-funktsii-formuvannia-vidpravlen/2-5-funktsiia-kalkuliator-calculateshipments
 * Ukrainian description for vars

 * @var string $ClientUID "Унікальний ідентифікатор клієнта, присвоюється після внесення контрагента в систему (Обов’язковий)"
 * @var bool $SenderService "Вид сервісу, якщо «0» - склад «1» - двері (Обов’язковий)"
 * @var string $SenderBranch_UID "Унікальний ідентифікатор підрозділу Відправника,  результат виконання функції «Branch» п.1.7. Встановлюється, якщо [SenderService]=0 (Якщо сервіс відправника склад - Обов’язковий)"
 * @var string $SenderCity_UID "Унікальний ідентифікатор міста Відправника,  результат виконання функції «City» п.1.5. Встановлюється,  якщо [SenderService]=1(Якщо сервіс відправника двері - Обов’язковий)"
 * @var bool $ReceiverService "Вид сервісу, якщо «0» - склад «1» - двері (Обов’язковий)"
 * @var string $ReceiverBranch_UID "Унікальний ідентифікатор підрозділу Одержувача,  результат виконання функції «Branch» п.1.7. Встановлюється, якщо [ReceiverService]=0 (Якщо сервіс відправника склад - Обов’язковий)"
 * @var string $ReceiverCity_UID "Унікальний ідентифікатор містаОдержувача,  результат виконання функції «City» п.1.5. Встановлюється,  якщо [ReceiverService]=1(Якщо сервіс отримувача двері - Обов’язковий)"
 * @var string $COD "COD"
 * @var string $ContitionsName "Унікальний ідентифікатор умови !  У блоці може бути один або декілька записів,  результат виконання функції «ShipmentOptCondition» п.1.10. "
 * @var string $SendingFormat "Код формату відправлення (Обов’язковий)! "Результат виконання функції «ShipmentFormats» п.1.9.У блоці може бути один або декілька записів. При формуванні місця слід виконуватип еревірку формату відправлення у відповідності до мінімальних та максимальних параметрів ваги, об’єму, страхування. (Обов’язковий)
 * @var string $Quantity "Кількість (Обов’язковий)"
 * @var string $Length "Довжина (Обов’язковий)"
 * @var string $Width "Ширина (Обов’язковий)"
 * @var string $Height "Висота (Обов’язковий)"
 * @var string $Weight "Вага (Обов’язковий)"
 * @var string $Packaging "Упаковка"
 * @var string $Insurance "Страхування (Обов’язковий)"
 */

?>

<CalculateShipment>
    <ClientUID><?php echo $ClientUID; ?></ClientUID>
    <SenderService><?php echo $SenderService; ?></SenderService>
    <SenderBranch_UID><?php echo $SenderBranch_UID; ?></SenderBranch_UID>
    <SenderCity_UID><?php echo $SenderCity_UID; ?></SenderCity_UID>
    <ReceiverService><?php echo $ReceiverService; ?></ReceiverService>
    <ReceiverBranch_UID><?php echo $ReceiverBranch_UID; ?></ReceiverBranch_UID>
    <ReceiverCity_UID><?php echo $ReceiverCity_UID; ?></ReceiverCity_UID>
    <COD><?php echo $COD; ?></COD>
    <Conditions_items>
        <ContitionsName><?php echo $ContitionsName; ?></ContitionsName>
    </Conditions_items>
    <Places_items>
        <SendingFormat><?php echo $SendingFormat; ?></SendingFormat>
        <Quantity><?php echo $Quantity; ?></Quantity>
        <Weight><?php echo $Weight; ?></Weight>
        <Length><?php echo $Length; ?></Length>
        <Width><?php echo $Width; ?></Width>
        <Height><?php echo $Height; ?></Height>
        <Packaging><?php echo $Packaging; ?></Packaging>
        <Insurance><?php echo $Insurance; ?></Insurance>
    </Places_items>

</CalculateShipment>

<?php

/**
 * @author Amass Advance <wearde.studio@gmail.com>
 * @link http://amass.pp.ua
 */

/**
 * @see https://wiki.meest-group.com/index.php/en/2-funktsii-formuvannia-vidpravlen/2-2-funktsiia-stvorennia-novoho-vidpravlenniamy-createshipments
 * Ukrainian description for vars
 * @var string $ClientsShipmentRef "Код (номер) відправлення, присвоюється контрагентом"
 * @var string $ClientUID "Унікальний ідентифікатор клієнта, присвоюється після внесення контрагента в систему (Обов’язковий)"
 * @var string $Sender "Відправник"
 * @var bool $SenderService "Вид сервісу, якщо «0» - склад «1» - двері (Обов’язковий)"
 * @var string $SenderBranch_UID "Унікальний ідентифікатор підрозділу Відправника,  результат виконання функції «Branch» п.1.7. Встановлюється, якщо [SenderService]=0 (Якщо сервіс відправника склад - Обов’язковий)"
 * @var string $SenderStreet_UID "Унікальний ідентифікатор вулиці (адреси) Відправника,  результат виконання функції«Аddress» п.1.6. Встановлюється,  якщо [SenderService]=1та [SenerAdressType]=0 (Якщо сервіс відправника двері - Обов’язковий)"
 * @var string $SenderHouse "Номер будинку адреси Відправника. Встановлюється,  якщо [SenderService]=1 (Якщо сервіс відправника двері - Обов’язковий)"
 * @var string $SenderFlat "Номер квартири Відправника. Встановлюється,  якщо [SenderService]=1"
 * @var string $SenderTel "Контактний телефонВідправника"
 * @var string $Receiver "Одержувач"
 * @var string $ReceiverService "Вид сервісу, якщо «0» - склад «1» - двері (Обов’язковий)"
 * @var string $ReceiverBranch_UID "Унікальний ідентифікатор підрозділу Одержувача,  результат виконання функції «Branch» п.1.7. Встановлюється, якщо [ReceiverService]=0 (Якщо сервіс відправника склад - Обов’язковий)"
 * @var string $ReceiverStreet_UID "Унікальний ідентифікатор вулиці (адреси) Одержувача,  результат виконання функції«Аddress» п.1.6. Встановлюється,  якщо [ReceiverService]=1 та [SenerAdressType]=0 (Якщо сервіс відправника двері - Обов’язковий)"
 * @var string $ReceiverHouse "Номер будинку адреси Одержувача. Встановлюється,  якщо[ReceiverService]=1(Якщо сервіс отримувача двері - Обов’язковий)"
 * @var string $ReceiverFlat "Номер квартири адреси Одержувача. Встановлюється,  якщо [ReceiverService]=1"
 * @var string $ReceiverTel "Контактний телефонОдержувача.(Обов’язковий)"
 * @var string $COD "COD"
 * @var string $Notation "Примітка"
 * @var string $Receiver_Pay "Спосіб оплати, якщо «0» - оплачує Відправник «1» - оплачує Одержувач (Обов’язковий)"
 * @var string $TypePay "Тип оплати,якщо «0» - безготівка «1» - готівка (Обов’язковий)"
 * @var string $ContitionsName "Унікальний ідентифікатор умови !  У блоці може бути один або декілька записів,  результат виконання функції «ShipmentOptCondition» п.1.10. "
 * @var string $SendingFormat "Код формату відправлення (Обов’язковий)! "Результат виконання функції «ShipmentFormats» п.1.9.У блоці може бути один або декілька записів. При формуванні місця слід виконуватип еревірку формату відправлення у відповідності до мінімальних та максимальних параметрів ваги, об’єму, страхування. (Обов’язковий)
 * @var string $Quantity "Кількість (Обов’язковий)"
 * @var string $Volume "Об’єм"
 * @var string $Weight "Вага (Обов’язковий)"
 * @var string $Packaging "Упаковка"
 * @var string $Insurance "Страхування (Обов’язковий)"
 */

?>
<Shipments>
    <CreateShipment>
        <ClientsShipmentRef><?php echo $ClientsShipmentRef; ?></ClientsShipmentRef>
        <ClientUID><?php echo $ClientUID; ?></ClientUID>
        <Sender><?php echo $Sender; ?></Sender>
        <SenderService><?php echo $SenderService; ?></SenderService>
        <SenderBranch_UID><?php echo $SenderBranch_UID; ?></SenderBranch_UID>
        <SenderStreet_UID><?php echo $SenderStreet_UID; ?></SenderStreet_UID>
        <SenderHouse><?php echo $SenderHouse; ?></SenderHouse>
        <SenderFlat><?php echo $SenderFlat; ?></SenderFlat>
        <SenderTel><?php echo $SenderTel; ?></SenderTel>
        <Receiver><?php echo $Receiver; ?></Receiver>
        <ReceiverService><?php echo $ReceiverService; ?></ReceiverService>
        <ReceiverBranch_UID><?php echo $ReceiverBranch_UID; ?></ReceiverBranch_UID>
        <ReceiverStreet_UID><?php echo $ReceiverStreet_UID; ?></ReceiverStreet_UID>
        <ReceiverHouse><?php echo $ReceiverHouse; ?></ReceiverHouse>
        <ReceiverFlat><?php echo $ReceiverFlat; ?></ReceiverFlat>
        <ReceiverTel><?php echo $ReceiverTel; ?></ReceiverTel>
        <COD><?php echo $COD; ?></COD>
        <Notation><?php echo $Notation; ?></Notation>
        <Receiver_Pay><?php echo $Receiver_Pay; ?></Receiver_Pay>
        <TypePay><?php echo $TypePay; ?></TypePay>
        <Conditions_items>
            <ContitionsName><?php echo $ContitionsName; ?></ContitionsName>
        </Conditions_items>
        <Places_items>
            <SendingFormat><?php echo $SendingFormat; ?></SendingFormat>
            <Quantity><?php echo $Quantity; ?></Quantity>
            <Volume><?php echo $Volume; ?></Volume>
            <Weight><?php echo $Weight; ?></Weight>
            <Packaging><?php echo $Packaging; ?></Packaging>
            <Insurance><?php echo $Insurance; ?></Insurance>
        </Places_items>
    </CreateShipment>
</Shipments>

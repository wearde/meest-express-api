<?php
/**
 * @author Amass Advance <wearde.studio@gmail.com>
 * @link http://amass.pp.ua
 */

/**
 * @see https://wiki.meest-group.com/index.php/en/2-funktsii-formuvannia-vidpravlen/2-4-funktsiia-reiestratsiia-vidpravlen-dlia-perevezennia-createregister
 * Ukrainian description for vars
 * @var string $ClientsShipmentRef "Номер відправлення (Обов’язковий)"
 * @var string $ClientUID "Унікальний ідентифікатор клієнта, присвоюється після внесення контрагента в систему (Обов’язковий)"
 * @var array $Shipments "Перелік відправлень. У блоці може бути один або декілька записів. (Обов’язковий)"
 */
?>

<CreateRegister>
    <ClientUID><?php echo $ClientUID; ?></ClientUID>
    <Shipments>
        <?php foreach($Shipments as $ClientsShipmentRef) : ?>
        <ClientsShipmentRef><?php echo $ClientsShipmentRef?></ClientsShipmentRef>
        <?php endforeach;?>
    </Shipments>
</CreateRegister>

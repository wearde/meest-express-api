<?php
/**
 * @author Amass Advance <wearde.studio@gmail.com>
 * @link http://amass.pp.ua
 */

/**
 * @see https://wiki.meest-group.com/index.php/en/2-funktsii-formuvannia-vidpravlen/2-3-funktsiia-vydalennia-ne-zareiestrovanoho-vidpravlennia-deleteshipments
 * Ukrainian description for vars
 * @var string $ClientsShipmentRef "Номер відправлення (Обов’язковий)"
 * @var string $ClientUID "Унікальний ідентифікатор клієнта, присвоюється після внесення контрагента в систему (Обов’язковий)"
 */
?>
<Shipments>
    <DeleteShipment>
        <ClientsShipmentRef><?php echo $ClientsShipmentRef; ?></ClientsShipmentRef>
        <ClientUID><?php echo $ClientUID; ?></ClientUID>
    </DeleteShipment>
</Shipments>
